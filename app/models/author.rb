class Author < ApplicationRecord
  
  extend FriendlyId
  friendly_id :last_name, use: [:slugged, :history]

  #Validations
  validates :first_name, :last_name, presence: true, allow_blank: true
  validates :last_name, uniqueness: true
  validates_format_of :last_name, :with => /\A[a-z0-9]+\z/i

  #Associations
  has_many :books

end
