class Book < ApplicationRecord
  
  #Call backs
  before_save :populate_short_name

  #Validations
  validates :full_name, presence: true, allow_blank: true

  #Associations
  belongs_to :author, optional: true

  private
  def populate_short_name
  	self.short_name = full_name.downcase.tr(" ", "_")
  end

end
