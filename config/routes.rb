Rails.application.routes.draw do

	root 'authors#new'
	get 'nice_books/all', to: 'books#index', :defaults => { :format => :json }
	get 'authors/new_author', to: 'authors#new'
	get 'authors/:id', to: 'authors#show', as: :author
	post 'authors', to: 'authors#create'


end
