Author.create!([
            {first_name: "John", last_name: "Rick"},
            {first_name: "Ahmed", last_name: "Hassan"},
            {first_name: "Mohamed", last_name: "Tarik"}
            ])
Book.create!([
			{full_name: "Jounrny to the center of the earth", year: "1963",author: Author.last},
			{full_name: "Macbeth", year: "1606", author: Author.first}
	])