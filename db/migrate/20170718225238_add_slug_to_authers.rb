class AddSlugToAuthers < ActiveRecord::Migration[5.1]
  def change
    add_column :authors, :slug, :string, index: true
  end
end
